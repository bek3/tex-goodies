#!/usr/bin/env bash

# TexGoodies - Install Script
# Created by Brendan Kristiansen on 18 July 2018
# Last Modified 09 January 2019

echo "Installing TexGoodies"

tg_install=$HOME/bin

if [ ! -d  $tg_install ]
then
	mkdir $tg_install
fi

if [ ! -e ~/.tg-config ]
then
	echo "Creating tg-congfig file"
	touch ~/.tg-config
	echo "tg_path=~/.tg" >> ~/.tg-config
	echo "tg_install=$HOME/bin" >> ~/.tg-config
	source ~/.tg-config
else
	echo "Using existing tg-config file"
fi

if [ ! -d scripts/ ]
then
	echo "Scripts directory not found."
else
	sudo cp -v scripts/tg-addMakefile.sh $tg_install/tg-addMakefile
	sudo cp -v scripts/tg-newTex.sh $tg_install/tg-newTex
	sudo cp -v scripts/tg-uninstall.sh $tg_install/tg-uninstall
	sudo cp -v scripts/tg-update.sh $tg_install/tg-update
	sudo chmod 777 $tg_install/tg-addMakefile
	sudo chmod 777 $tg_install/tg-newTex
	sudo chmod 777 $tg_install/tg-uninstall
	sudo chmod 777 $tg_install/tg-update
fi

if [ ! -d resources ]
then
	echo "Resources directory not found"
else
	mkdir -v $tg_path
	mkdir $tg_path/rss/
	sudo cp -v resources/Makefile $tg_path/rss/
	sudo cp -v resources/blank.tex $tg_path/rss/
	sudo cp -v resources/common-sty.sty $tg_path/rss/
fi
