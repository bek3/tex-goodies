# TexGoodies
### A useful set of scripts for working with LaTeX

TexGoodies contains the following useful resources for the average LaTeX user:

Scripts:

* tg-addMakefile: If your current working directory has a .tex file in it, run this to copy in a LaTeX Makefile.
* tg-newTex: Run with the desired name of your document as an argument and it will create a new directory with a
	Makefile, and blank .tex file.

LaTeX resources:

* common-sty.sty: A stylesheet that contains all of the packages and macros I usually use when typesetting LaTeX.

### Don't like TexGoodies?

Uninstall by running 'tg-uninstall' and let me know what can be improved!
