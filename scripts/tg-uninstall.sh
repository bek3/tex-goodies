#!/usr/bin/env bash

# TexGoodies - uninstall Script
# Created by Brendan Kristiansen on 18 July 2018
# Last Modified 09 January 2019

source ~/.tg-config

echo "unistalling TexGoodies"

sudo rm -v $tg_install/tg-*

sudo rm -r -v $tg_path
sudo rm -v ~/.tg-config
