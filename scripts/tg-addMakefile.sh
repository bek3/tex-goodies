#!/usr/bin/env bash

# TexGoodies - Add Makefile
# Created by Brendan Kristiansen on 18 July 2018
# Last Modified 09 January 2019

source ~/.tg-config

if [ ! -e Makefile ]
then
	if [ -e *.tex ]
	then
		cp -v ~/.tg/rss/Makefile .
	else
		echo "No .tex document found!"
	fi
else
	echo "Makefile already found!"
fi
