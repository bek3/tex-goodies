#!/usr/bin/env bash

# TexGoodies - New LaTeX Document
# Created by Brendan Kristiansen on 18 July 2018
# Last Modified 09 January 2019

source ~/.tg-config

if [ $# -eq 0 ]
then
	echo "Give name of document as argument"
elif [ $# -eq 1 ]
then
	doc_name=$1
	mkdir $doc_name/

	cd $doc_name/
	
	cp $tg_path/rss/blank.tex $doc_name.tex
	cp $tg_path/rss/common-sty.sty .

	tg-addMakefile

else
	echo "Invalid number of arguments"
fi
